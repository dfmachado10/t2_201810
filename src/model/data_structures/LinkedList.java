package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class LinkedList<T>{

	private Node<T> first;
	
	private Node<T> last;
	
	public LinkedList(){
		
	}
	
	public Node<T> getFirst() {
		return first;
	}
	
	public Node<T> getLast(){
		return last;
	}
	
	public void addAsFirst(T info){
		if(first==null) {
			first = new Node<T>(info, null);
			last=first;
		}
		
		else{
			Node<T> temp= first;
			Node<T> n= new Node<T>(info, temp);
			first=n;
		}
	}
	
	public void addAtEnd(T info){
		
		if(first==null){
			addAsFirst(info);
		}
		else{
			Node<T> temp=first;
			while(temp.getNext()!= null) temp= temp.getNext();
			
			temp.setNext(new Node<T>(info, null));
			last=temp.getNext();
		}
	}
	
	public void insertBefore(T key, T toInsert)
	{
	   if(first.info.equals(key))
	   {
	      addAsFirst(toInsert);
	      return;
	   }

	   Node<T> prev = null;
	   Node<T> cur = first;

	   while(cur != null && !cur.info.equals(key))
	   {
	      prev = cur;
	      cur = cur.next;
	   }
	   //insert between cur and prev
	   if(cur != null) prev.next = new Node<T>(toInsert, cur);
	}
	
	public void insertAfter(T key, T toInsert)
	{
	   Node<T> tmp = first;
	   while(tmp != null && !tmp.info.equals(key)) tmp = tmp.next;

	   if(tmp != null)
	      tmp.next = new Node<T>(toInsert, tmp.next);
	}
	
	public void remove(T key)
	{
	   if(first == null) 

	   if( first.info.equals(key) )
	   {
	      first = first.next;
	      return;
	   }

	   Node<T> cur  = first;
	   Node<T> prev = null;

	   while(cur != null && !cur.info.equals(key) )
	   {
	      prev = cur;
	      cur = cur.next;
	   }



	   prev.next = cur.next;
	}

	public Iterator<Node<T>> iterator(){
		
		ArrayList<Node<T>> a = new ArrayList<Node<T>>();
		Node<T> temp= first.next;
		while(temp.next !=null){
			a.add(temp);
			temp= temp.next;
		}
		return a.iterator();
		
	}


	private static class Node<T>{
		
		private T info;
		private Node<T> next;
		
		public Node(T pInfo,Node<T> pNext){
			 
			info=pInfo;
			next=pNext;
		}
		
		public T getInfo(){
			return info;
		}
		
		public Node<T> getNext(){
			return next;
		}
		
		public void setNext(Node<T> pNext){
			next=pNext;
		}
		
		
	}

}
